#todo_list.rb

:NEW
:CANCELED
:DONE

class Todo
	attr_accessor :content, :status

	def initialize(content)
		@content = content
		@state = :NEW
	end

	def to_s
		@content.to_s + " | " + @state.to_s
	end

	def cancel
		@state = :CANCELED
	end

	def done
		@state = :DONE
	end
end

class TodoList
	attr_accessor :array

	def initialize
		@array = [];
	end

	def add(todo)
		@array.push(todo)
	end

	def cancel(index)
		@array[index].cancel
	end

	def done(index)
		@array[index].done
	end

	def to_s
		s = ''
		if array.empty?
			s = "List is empty!"
		else
		@array.each_with_index do |todo, index|
			s += "#{index+1}) - " + todo.to_s + "\n"
		end
	end
		s
	end
end

checker = true
agreement_checker = true
todoList = TodoList.new

while checker
	puts "Do you want to add new TODO or check one of existing? [a = add | c = check | e = exit]"

while true
 		agreement = gets.chomp
 		break if agreement.eql?("a") || agreement.eql?("c") || agreement.eql?("e")
end

case agreement
  when "a"
    print "Please, enter your TODO: "
    content = gets.chomp
    todoList.add(Todo.new(content))
    puts "TODO added successfully."
  	when "c"
    	if todoList.array.empty?
    		puts "There is nothing to check :("
    	else
    		puts todoList

			while true
				puts "Input number from 1-#{todoList.array.length}"
			begin
				print "Choose number of TODO you want to check: "
				i = Integer(gets.chomp)
				rescue
				puts 'Please enter an integer value!'
				retry
				end

				break if i > 0 && i <= todoList.array.length
			end
    
			puts "Do you want to check it CANCEL or DONE?[c = CANCEL|d = DONE]"

			while true
 				status = gets.chomp
 				break if status.eql?("c") || status.eql?("d")
			end

			case status
			when "c" 
				todoList.cancel(i - 1)
			when "d" 
				todoList.done(i - 1)
			end

			puts "Operation done successfully."
		end
  when "e"
    checker = false
    puts "Your TODO:"
    puts todoList
	end
end