#oop_model.rb

class Human
	attr_accessor :name, :invited, :drink

	def initialize(name)
		@name = name
	end

	def to_s
		@name.capitalize
	end

	def invite(other)
    	@invited = other
    	return self
  	end

  	def to_drink(drink)
  		@drink = drink
  		puts "#{@invited} agrees to drink some #{@drink} with you!"
  	end
end

class Drink
	attr_accessor :name

	def initialize(name)
		@name = name.capitalize
	end

	def to_s
		@name
	end
end

puts "Meeting orginizer. v.1.0"
checker = true
human1 = Human.new "Volodya"
human2 = Human.new "Ira"
drink = Drink.new "Coffee"

while checker
	puts "Orginize meeting of #{human1} and #{human2}"
 	human1.invite(human2).to_drink drink
 	puts "Do you want to organize another meeting? [y = yes | n = no]"

 	while true
 		agreement = gets.chomp
 		break if agreement.eql?("y") || agreement.eql?("n") || agreement.eql?("yes") || agreement.eql?("no")
 	end

 	if agreement.include? ("y")
 		print "Name of invitor: "
 		human1.name = gets.chomp
 		print "Name of invited: "
 		human2.name = gets.chomp
 		puts "What you would like to drink? "
 		drink.name = gets.chomp
 	else
 		checker = false
 	end 
end