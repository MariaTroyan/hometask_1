# calculator.rb 

puts "Help: Calculator for simple math operations: +, -, *, /."

loop do

begin
	print "Enter A:" 
	a = Integer(gets.chomp) 
rescue
  p "Please enter an integer value:"
retry
end

begin
	print "Enter B:" 
	b = Integer(gets.chomp)
rescue
	p "Please enter an integer value:" 
retry
end

checker = true
while (checker)
	print "Enter operation:" 
	operation = gets.chomp 
if operation == "+" 
	puts "A + B = #{a + b}"
	checker = false
elsif operation == "-"
	puts "A - B = #{a - b}"
	checker = false
elsif operation == "*"
	puts "A * B = #{a * b}"
	checker = false
elsif operation == "/"
	puts "A / B = #{a / b}"
	checker = false
else puts "Error! Wrong operation! Avaliable operations: +, -, *, /."
end
end

	puts "Press q to quit and anything else to continue."
	exit_condition = gets.chomp
break if exit_condition.eql?("q")
end